import base64
import logging
import random
import re
import typing
import datetime
from collections import defaultdict
from enum import auto, Enum

import requests

from intratime.config import Config

logger = logging.getLogger("cli")


class URL_WEB:
    BASE = "https://panel.intratime.es"
    LOGIN = BASE + "/login/fichaje_web"
    ENTRY = BASE + "/login/fichaje_web_log"

    @classmethod
    def clocking(cls, action: str, user_id: int, token: str, coords: typing.Tuple[float, float]) -> str:
        encoded_coords = base64.b64encode(f"{coords[0]:.7f},{coords[1]:.7f}".encode()).decode()
        return f"{cls.ENTRY}/{user_id}/{action}/{token}/{encoded_coords}"


class URL_API:
    BASE = "http://newapi.intratime.es/api"
    LOGIN = BASE + "/user/login"
    CLOCKINGS = BASE + "/user/clockings"


class WorkingStatus(Enum):
    OFF = auto()
    WORKING = auto()
    PAUSE = auto()


class DaySummary(typing.NamedTuple):
    start: typing.Optional[datetime.datetime]
    pause: typing.List[typing.Tuple[datetime.datetime, typing.Optional[datetime.datetime]]]
    stop: typing.Optional[datetime.datetime]
    working_time: datetime.timedelta
    status: WorkingStatus


WeekSummary = typing.Dict[datetime.datetime, DaySummary]


class Client:
    def __init__(self, config_file: typing.Optional[str] = None):
        config = Config(config_file)
        self.user = config("user")
        self.pin = config("pin")
        self.latitude = config("latitude", cast=float)
        self.longitude = config("longitude", cast=float)
        self.user_agent = config("user_agent")

        self.session = requests.Session()
        self.session.cookies.clear()

    @property
    def coords(self) -> typing.Tuple[float, float]:
        return (
            random.uniform(self.latitude - 0.0001, self.latitude + 0.0001),
            random.uniform(self.longitude - 0.0001, self.longitude + 0.0001)
        )

    @property
    def auth_payload(self) -> typing.Dict[str, str]:
        return {"user_EMAIL": self.user, "user_PIN": self.pin}

    @property
    def web_headers(self) -> typing.Dict[str, str]:
        return {
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-GB,en-US;q=0.9,en;q=0.8,es;q=0.7",
            "Origin": URL_WEB.BASE,
            "Referer": URL_WEB.LOGIN,
            "User-Agent": self.user_agent,
        }

    def login_api(self) -> typing.Dict[str, typing.Union[str, int]]:
        headers = {"Accept": "application/vnd.apiintratime.v1+json"}
        data = {"user": self.user, "pin": self.pin}
        with self.session.post(URL_API.LOGIN, data=data, headers=headers) as response:
            body = response.json()

            return {
                "id": body["USER_ID"],
                "name": body["USER_NAME"],
                "username": body["USER_USERNAME"],
                "token": body["USER_TOKEN"],
                "last": None,
            }

    def login_web(self) -> typing.Dict[str, typing.Union[str, int]]:
        with self.session.post(URL_WEB.LOGIN, data=self.auth_payload, headers=self.web_headers) as response:
            body = response.text

            logger.debug("Login response: %s", body)

            return {
                "id": self._get_var(body, "iUser_id", int),
                "token": self._get_var(body, "sToken"),
                "last": self._get_var(body, "iLastFichaje", int),
            }

    def start(self) -> typing.Dict:
        return self._clocking(action="checkin")

    def stop(self) -> typing.Dict:
        return self._clocking(action="checkout")

    def pause(self) -> typing.Dict:
        return self._clocking(action="ck_break")

    def restart(self) -> typing.Dict:
        return self._clocking(action="ck_back")

    def today(self) -> DaySummary:
        token = self.login_api()["token"]

        date_from = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
        date_to = datetime.datetime.now().replace(hour=23, minute=59, second=59, microsecond=0)
        data = {"from": date_from.strftime("%Y-%m-%d %H:%M:%S"), "to": date_to.strftime("%Y-%m-%d %H:%M:%S")}
        headers = {"Accept": "application/vnd.apiintratime.v1+json", "token": token}

        with self.session.get(URL_API.CLOCKINGS, params=data, headers=headers) as response:
            entries = response.json()
            entries.reverse()

            return self._day_summary(entries)

    def week(self) -> WeekSummary:
        token = self.login_api()["token"]

        now = datetime.datetime.now()
        date_from = (now - datetime.timedelta(days=now.weekday())).replace(hour=0, minute=0, second=0, microsecond=0)
        date_to = (date_from + datetime.timedelta(days=6)).replace(hour=23, minute=59, second=59, microsecond=0)
        data = {"from": date_from.strftime("%Y-%m-%d %H:%M:%S"), "to": date_to.strftime("%Y-%m-%d %H:%M:%S")}
        headers = {"Accept": "application/vnd.apiintratime.v1+json", "token": token}

        with self.session.get(URL_API.CLOCKINGS, params=data, headers=headers) as response:
            entries = response.json()
            entries.reverse()

            entries_by_date = defaultdict(list)
            for entry in entries:
                entries_by_date[self._parse_date(entry["INOUT_DATE"]).date()].append(entry)

            return {date: self._day_summary(entries) for date, entries in entries_by_date.items()}

    def _get_var(self, text: str, name: str, type_value: typing.Any = str):
        regex = re.compile(rf"var (?P<name>{name})\s*=\s*'?(?P<value>.*?)'?;")

        try:
            return type_value(regex.search(text).group("value"))
        except AttributeError:
            return None

    def _parse_date(self, date_str):
        return datetime.datetime.strptime(date_str, "%Y-%m-%d %H:%M:%S")

    def _day_summary(self, entries) -> DaySummary:
        summary = {"start": None, "pause": [], "stop": None, "working_time": None, "status": None}

        # Calculate start, stop and pauses
        current_pause = None
        for entry in entries:
            if entry["INOUT_TYPE"] == 0 and not summary["start"]:
                summary["start"] = self._parse_date(entry["INOUT_DATE"])
            elif entry["INOUT_TYPE"] == 1 and not summary["stop"]:
                summary["stop"] = self._parse_date(entry["INOUT_DATE"])
            elif entry["INOUT_TYPE"] == 2 and not current_pause:
                current_pause = self._parse_date(entry["INOUT_DATE"])
            elif entry["INOUT_TYPE"] == 3 and current_pause:
                summary["pause"].append((current_pause, self._parse_date(entry["INOUT_DATE"])))
                current_pause = None

        if current_pause is not None:
            summary["pause"].append((current_pause, None))

        # Infer status
        if summary["stop"] or not summary["start"]:
            summary["status"] = WorkingStatus.OFF
        elif summary["pause"] and summary["pause"][-1][1] is None:
            summary["status"] = WorkingStatus.PAUSE
        else:
            summary["status"] = WorkingStatus.WORKING

        # Calculate working time
        if summary["start"]:
            stop_time = summary["stop"] or datetime.datetime.now()
            pause_time = sum([i[1] - i[0] for i in summary["pause"] if i[1] is not None], datetime.timedelta())
            working_time = stop_time - summary["start"] - pause_time
            summary["working_time"] = datetime.timedelta(seconds=int(working_time.total_seconds()))

        return DaySummary(**summary)

    def _clocking(self, action: str) -> typing.Dict:
        login = self.login_web()

        url = URL_WEB.clocking(action=action, user_id=login["id"], token=login["token"], coords=self.coords)
        logger.debug("Request to %s", url)
        with self.session.post(url=url, data=self.auth_payload, headers=self.web_headers) as response:
            try:
                response.encoding = "utf-8-sig"
                return response.json()
            except:
                logger.exception("Wrong response: %s", response.text)
