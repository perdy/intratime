import argparse
import datetime
import logging
import os
import sys

from clinner.command import command, Type
from clinner.inputs import default_input
from clinner.run import Main

from intratime.client import Client

logger = logging.getLogger("cli")


@command(command_type=Type.PYTHON, parser_opts={"help": "Configure it"})
def config(*args, **kwargs):
    values = {
        "user": input("User: "),
        "pin": input("Pin: "),
        "latitude": default_input("Latitude: ", default="36.7373850"),
        "longitude": default_input("Longitude: ", default="-4.5523663"),
        "user_agent": default_input("User agent: ", default="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) "
                                                            "AppleWebKit/537.36 (KHTML, like Gecko) "
                                                            "Chrome/74.0.3729.131 Safari/537.36")
    }

    os.makedirs(os.path.dirname(kwargs["config"]), exist_ok=True)
    with open(kwargs["config"], "w") as output:
        output.write("\n".join([f"{k}={v}" for k, v in values.items()]))


@command(command_type=Type.PYTHON, parser_opts={"help": "Check login"})
def login(*args, **kwargs):
    client = Client(kwargs["config"])

    data = client.login_web()

    logger.info(f"User successfully logged in with ID '{data['id']:d}' and token '{data['token']}'")


@command(command_type=Type.PYTHON, parser_opts={"help": "Start working time"})
def start(*args, **kwargs):
    client = Client(kwargs["config"])

    data = client.start()

    if not data or not data.get('bFichajeOK'):
        logger.error("Error starting working time")
        return 1

    logger.info("Working time started successfully")


@command(command_type=Type.PYTHON, parser_opts={"help": "Stop working time"})
def stop(*args, **kwargs):
    client = Client(kwargs["config"])

    data = client.stop()

    if not data or not data.get('bFichajeOK'):
        logger.error("Error stopped working time")
        return 1

    logger.info("Working time stopped successfully")


@command(command_type=Type.PYTHON, parser_opts={"help": "Pause working time"})
def pause(*args, **kwargs):
    client = Client(kwargs["config"])

    data = client.pause()

    if not data or not data.get('bFichajeOK'):
        logger.error("Error pausing working time")
        return 1

    logger.info("Working time paused successfully")


@command(command_type=Type.PYTHON, parser_opts={"help": "Restart after pause"})
def resume(*args, **kwargs):
    client = Client(kwargs["config"])

    data = client.restart()

    if not data or not data.get('bFichajeOK'):
        logger.error("Error resuming working time")
        return 1

    logger.info("Working time resumed successfully")


@command(command_type=Type.PYTHON, parser_opts={"help": "Check today's working time"})
def today(*args, **kwargs):
    client = Client(kwargs["config"])

    summary = client.today()

    s = f"Working time summary:\n" \
        f" - Start: {summary.start.time().isoformat()}\n"

    for entry in summary.pause:
        s += f" - Pause: {entry[0].time().isoformat()} - {entry[1].time().isoformat() if entry[1] else ''}\n"

    if summary.stop:
        s += f" - Stop:  {summary.stop.time().isoformat()}\n"

    s += f" - Total: {summary.working_time.total_seconds() // 3600} hours"
    if summary.working_time.total_seconds() % 3600:
        s += f" {summary.working_time.total_seconds() % 3600 // 60} minutes"


    logger.info(s)


@command(command_type=Type.PYTHON, parser_opts={"help": "Check this week's working time"})
def week(*args, **kwargs):
    client = Client(kwargs["config"])

    data = client.week()

    s = ""
    for day, summary in sorted(data.items()):
        s += f"{day.strftime('%A')}:\n" \
            f" - Start: {summary.start.time().isoformat()}\n"

        for entry in summary.pause:
            s += f" - Pause: {entry[0].time().isoformat()} - {entry[1].time().isoformat() if entry[1] else ''}\n"

        if summary.stop:
            s += f" - Stop:  {summary.stop.time().isoformat()}\n"

        s += f" - Total: {summary.working_time}\n"

    week_total = sum([i.working_time for i in data.values()], datetime.timedelta())
    s += f"Week Total: {week_total.total_seconds() // 3600} hours"
    if week_total.total_seconds() % 3600:
        s += f" {week_total.total_seconds() % 3600 // 60} minutes"

    logger.info(s)


class Path(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        setattr(namespace, self.dest, os.path.realpath(os.path.expandvars(values)))


class Intratime(Main):
    commands = ("config", "login", "start", "stop", "pause", "resume", "today", "week")

    def add_arguments(self, parser):
        verbose_group = parser.add_mutually_exclusive_group()
        verbose_group.add_argument(
            "-q", "--quiet", action="store_true", help="Quiet mode. No standard output other than executed application"
        )
        verbose_group.add_argument(
            "-v", "--verbose", action="count", default=1, help="Verbose level (This option is additive)"
        )
        parser.add_argument(
            "-c", "--config", help="Config file", default=os.path.expandvars("$HOME/.intratime/config"), action=Path
        )


def main():
    return Intratime().run()


if __name__ == "__main__":
    sys.exit(main())
