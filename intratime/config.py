import os
import typing

Undefined = typing.NewType("Undefined", object)


class Config:
    def __init__(self, env_file: str = None) -> None:
        self.file_values = {}  # type: typing.Dict[str, str]
        if env_file is not None and os.path.isfile(env_file):
            self.file_values = self._read_file(env_file)

    def __call__(self, key: str, cast: type=None, default: typing.Optional[typing.Any] = Undefined) -> typing.Any:
        return self.get(key, cast, default)

    def get(self, key: str, cast: type=None, default: typing.Any = Undefined) -> typing.Any:
        if key in os.environ:
            return self._perform_cast(key, os.environ[key], cast)
        if key in self.file_values:
            return self._perform_cast(key, self.file_values[key], cast)
        if default is not Undefined:
            return self._perform_cast(key, default, cast)
        raise KeyError(f"Config '{key}' is missing, and has no default.")

    def _read_file(self, file_name: str) -> typing.Dict[str, str]:
        file_values = {}  # type: typing.Dict[str, str]

        with open(file_name) as input_file:
            for line in input_file.readlines():
                line = line.strip()
                if "=" in line and not line.startswith("#"):
                    key, value = line.split("=", 1)
                    key = key.strip()
                    value = value.strip().strip("\"'")
                    file_values[key] = value

        return file_values

    def _perform_cast(self, key: str, value: typing.Any, cast: type = None) -> typing.Any:
        if cast is None or value is None:
            return value
        elif cast is bool and isinstance(value, str):
            mapping = {"true": True, "1": True, "false": False, "0": False}
            value = value.lower()
            if value not in mapping:
                raise ValueError(
                    f"Config '{key}' has value '{value}'. Not a valid bool."
                )
            return mapping[value]
        try:
            return cast(value)
        except (TypeError, ValueError):
            raise ValueError(
                f"Config '{key}' has value '{value}'. Not a valid {cast.__name__}."
            )
